

//Service for no reason yet :P
angular.module('pixel.services', []).service('pixelService', ['$http',function ($http) {
    {
        this.getImageUrl = function () {
            var url = '/api/getImageUrl';
            return $http.get(url);
        }
        this.getCategory= function () {
            var url = '/api/getCategoryNames';
            return $http.get(url);
        }

    }

}]);
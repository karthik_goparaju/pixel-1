angular.module('pixel.controllers', [])
    .controller("pixelController", ['$scope', 'pixelService', '$http', function ($scope, pixelService, $http) {

        /////////////////FETCH API DATA
        loadImages();
        loadCategories();


     
       

        //GLOBAL VARIABLES FOR MODAL FLAGS
        $scope.showModalText = false;
        $scope.showModalElement = false;
        $scope.showModalCategory = false;
        $scope.buttonClicked = "";

        ////////////////////////////////////////////////////MODAL FUNCTION FLAG HANDLING
        $scope.toggleModal = function (btnClicked) {
            $scope.buttonClicked = btnClicked;

            if (btnClicked == "ADD TEXT") {
                $scope.showModalText = !$scope.showModalText;
            }

            if (btnClicked == "ADD ELEMENT") {
                $scope.showModalElement = !$scope.showModalElement;
            }

            if (btnClicked == "REMOVE ELEMENT") {
                $scope.removeElement();
            }

            if (btnClicked == "CATEGORIES") {
                $scope.showModalCategory = !$scope.showModalCategory;
            }

            if (btnClicked == "THANKSGIVING") {
                $scope.getCategory("Thanksgiving");
            }

            if (btnClicked == "BIRTHDAY") {
                $scope.getCategory("Birthday");
            }

            if (btnClicked == "WEDDING") {
                $scope.getCategory("Wedding");
            }
        };



        //SET FABRIC PROPERTIES
        fabric.Object.prototype.set({
            centeredScaling: true,
            cornerStyle: 'circle',
            borderColor: 'green',
            cornerColor: 'rgba(41, 181, 168, 1)',
            transparentCorners: false,
            cornerStrokeColor: 'rgba(41, 181, 168, 1)',
            borderDashArray: [2,2]
        });

        //SET CANVAS PROPERTY VARIABLES
        var canvas = new fabric.Canvas('canvas');
        canvas.width = 1080;              // actual size given with integer values
        canvas.height = 1920;
        canvas.selectionColor = 'rgba(41, 181, 168, .3)';
        



        /********************************* GET TEXT**********/
        //Get text from pop up 
        $scope.getText = function (event) {

            //Create text box
            var textbox = new fabric.IText('Click to Edit', {
                left: 50,
                top: 50,
                fontFamily: 'Helvetica',
                fill: '#333',
                lineHeight: 1.1,

            });

            textbox.setControlsVisibility({
                mr: false,
                ml: false,
                mt: false,
                mb: false,
            });

            //add to canvas
            canvas.add(textbox).setActiveObject(textbox);

            //load font with font id came 
            loadAndUse(event.target.id);


        }
        //FUNCTION FOR LOADING FONT
        function loadAndUse(font) {
            var myfont = new FontFaceObserver(font)
            myfont.load()
              .then(function () {
                  // when font is loaded, use it.
                  canvas.getActiveObject().set("fontFamily", font);
                  canvas.requestRenderAll();
              }).catch(function (e) {
                  console.log(e)
                  alert('Please select and editable text font loading failed ' + font);
              });
        }
        /********************* GET ELEMENT ***********************/
        $scope.getElement = function (url) {
            fabric.Image.fromURL(url, function (Img) {
                Img.scaleToWidth(200);
                Img.scaleToHeight(300);
                Img.setControlsVisibility({
                    mr: false,
                    ml: false,
                    mt: false,
                    mb: false,
                });

                canvas.add(Img);

            });
        }
        /****************************************UPLOAD IMAGE TO CANVAS*************/


        document.getElementById('imgLoader').onchange = function handleImage(e) {
            var reader = new FileReader();
            reader.onload = function (event) {
                console.log('fdsf');
                var imgObj = new Image();
                imgObj.src = event.target.result;
                imgObj.onload = function () {
                    // start fabricJS stuff

                    var image = new fabric.Image(imgObj);
                    image.set({
                        top: 250,
                    });
                    Img.scaleToWidth(200);
                    Img.scaleToHeight(300);
                    image.setControlsVisibility({
                        mr: false,
                        ml: false,
                        mt: false,
                        mb: false,
                    });
                    
                   //
                    canvas.add(image);
                   

                    // end fabricJS stuff
                }

            }
            reader.readAsDataURL(e.target.files[0]);
        }




        /************************** CHANGE FONT OF EDITABLE TEXT*************/

        $scope.getFont = function (event) {
            loadAndUse(event.target.id); //USING PREVIOUS FUNCTION OF FONT ASSIGNING IN CANVAS
        }


        /**************************** Remove Element on canvas *****************/

        $scope.removeElement = function () {
            console.log("removed");
            //
            if (canvas.getActiveObject()) {
                //for single element deletion
                canvas.remove(canvas.getActiveObject());

            }
          
        }
       
        /*********************************Cahnge color of active object ********/

        $scope.setObjectColor = function (color) {

            if (canvas.getActiveObject() != null) {
                if (canvas.getActiveObject().get('type') == 'i-text') {
                    canvas.getActiveObject().set({ fill: color })
                    canvas.renderAll();
                }
            }
        }
        /*****************************************SHOW PREVIEW TO CANVAS*******/

        $scope.preview = false;

        $scope.showPreview = function () {


            //if preview button is on then only place image
            if (!$scope.preview) {
                fabric.Image.fromURL('/static/images/templates/scratch.jpg', function (prevImg) {

                    //code to fetch category based preview based on selection 
                    //from api yet to be doen :P 
                    //probably make an api which will fetch preview based on the template category selected

                    prevImg.scaleToWidth(351);
                    prevImg.scaleToHeight(624);
                    $scope.prevImg = prevImg;
                    prevImg.set({ selectable: false })

                    canvas.add(prevImg);
                    canvas.sendToBack(prevImg);

                });

                $scope.preview = true;  //preview on
            }
            else {

                canvas.remove($scope.prevImg);
                $scope.preview = false;         //preview off
            }



        }

        /***************************Add image to canvas**************/

        $scope.background = false;
        $scope.myImg = {};
        $scope.addImage = function (url) {

            if ($scope.background) {
                canvas.remove($scope.myImg);
            }

            if (url == '/static/images/templates/scratch.jpg') {
                //Remove every template if scratch is called
                canvas.remove($scope.myImg);
            }
            else {
                fabric.Image.fromURL(url, function (myImg) {

                    //code to fetch filter based on selection 
                    //from api yet to be doen :P 
                    //probably make an api which will fetch filter based on the template selected

                    myImg.scaleToWidth(351);
                    myImg.scaleToHeight(624);
                    $scope.myImg = myImg;
                    myImg.set({ selectable: false })

                    canvas.add(myImg);
                    canvas.sendToBack(myImg);
                    //bug resolved :)
                    $scope.background = true;

                });
            }
        }

        /****************************CATEGORY MODAL CHANGE EVENT**********/
        //Category Flag
        $scope.categoryClicked = false;
        //flag to show category on header of template which is selected (handled through angular)
        $scope.selectedCategory = "";
        //CAtegory tagline to show on header = comes from db table of category assigned in local object
        $scope.categoryTagline = "";

        $scope.getCategory = function (category) {

            //SCategory header appears 
            $scope.categoryClicked = true;      


            //Assign cateogry which called
            $scope.selectedCategory = category;


            //Assign tagline from db extracting from db table of category
            $scope.categoryTagline = " CHECK ";
                        
            console.log("category clicked  "+category+"  this");



        }














        /**********************************LOADING ANIMATION GIF FUNCTION**********/
        //loading flag
        $scope.loading = false;


        /**********************************API CALLS****************/
        //LOADING IMAGES THROUGHOUT THE APPLICATION API CALLS
        function loadImages(category) {
            pixelService.getImageUrl(category).then(function (response) {
                //local variables                  
                var count = 0;
                var data = [];

                //for template images
                var urls = response.data.images;
                $scope.arrayOfArrays = [];
                for (var i = 0; i < urls.length; i++) {
                    data.push(urls[i]);
                    count++;
                    if (count == 3 || i == urls.length - 1) {
                        $scope.arrayOfArrays.push(data);
                        count = 0;
                        data = [];
                    }
                }

                //for element images
                var elements = response.data.elements;
                $scope.arrayOfElements = [];
                for (var i = 0; i < elements.length; i++) {
                    data.push(elements[i]);
                    count++;
                    if (count == 3 || i == elements.length - 1) {
                        $scope.arrayOfElements.push(data);
                        count = 0;
                        data = [];
                    }
                }

            });

        }

        //LOADING CATEOGRY LIST IN ARRAY
        $scope.categories = [];
        function loadCategories() {
            pixelService.getCategory().then(function (response) {
               // $scope.categories = ['Thanksgiving', 'Birthday', 'Wedding', 'Bachelorette', 'Christmas', 'BridalShower', 'Celebration', 'Business', 'Baby Shower', 'College', 'Kids', 'Generic'];
                $scope.categories = response.data;

                //load categories with their tag line (make json) and assign it to object which can be used in diffrent functions 
                console.log($scope.categories);
            });
        }
      

    }]);


